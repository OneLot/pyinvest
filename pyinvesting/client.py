import datetime
import json
import re
import bs4
import cachetools.func
import pandas as pd
import pytz
import requests
import pyinvesting
from pyinvesting.bond import Bond
from pyinvesting.index import Index
from typing import List, Optional
from dateutil import relativedelta
from http import cookiejar


class CookieBlockAll(cookiejar.CookiePolicy):
    return_ok = set_ok = domain_return_ok = path_return_ok = lambda self, *args, **kwargs: False
    netscape = True
    rfc2965 = hide_cookie2 = False


class Client(object):
    _user_agent = "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36"
    futures_specs_urls = [
        'https://www.investing.com/indices/Service/SpecificationsFutures?pairid=0&sid=785570e03c27af532af0f65605f38af&filterParams=&smlID=70',
        'https://www.investing.com/commodities/Service/Specifications?pairid=0&sid=7a3b58eb7cfb629ec9b32841c86e0360&filterParams=&smlID=182',
        'https://www.investing.com/ratesbonds/Service/SpecificationsFutures?pairid=0&sid=4198194e499a8f72007436da623be59b&filterParams=&smlID=200000',
    ]
    _extended_bond_url_by_iso2_country = {
        'us': "/usa-government-bonds",
        'uk': "/uk-government-bonds",
        'de': "/germany-government-bonds",
        "jp": "/japan-government-bonds",
        "hk": "/hong-kong-government-bonds"
    }

    def __init__(self,
                 indexman_base):
        self.symbols = {}
        self.bonds = {}
        self._client = requests.Session()
        self._client.cookies.set_policy(CookieBlockAll)
        self._client.headers = {
            'user-agent': self._user_agent,
            'X-Requested-With': 'XMLHttpRequest',
            'Referer': 'https://www.investing.com',
        }
        self._get_futures_specs()
        self.indexman_base = indexman_base

    @cachetools.func.lfu_cache(maxsize=500, typed=False)
    def get_symbol(self, name):
        print(str(datetime.datetime.now()) + 'getting symbol' + name)
        # If we already have it, just return it
        if name in self.symbols.keys():
            return self.symbols[name]

        # Extract exchange if we got it
        if ':' in name:
            (exchange, ticker) = name.split(':')
        else:
            ticker = name
            exchange = None

        # Search for a cached future contract without exchange name
        if exchange is None and re.search(r'^[a-z0-9]+$', ticker):
            for s in self.symbols.values():
                contract = s.name.split(':')[1]
                if contract == ticker:
                    return s

        sym_data = self.search(ticker, exchange)

        if sym_data:
            s = pyinvesting.Symbol(
                name=sym_data['symbol'].lower(),
                info=sym_data['name'],
                url='https://www.investing.com%s' % sym_data['link'],
                sym_type=sym_data['pair_type'],
                client=self._client,
            )

            return s
        else:
            return

    def get_bonds(self, region: str) -> Optional[List[Bond]]:
        if region.lower() not in self._extended_bond_url_by_iso2_country.keys():
            return
        else:
            url = 'https://www.investing.com/rates-bonds%s' % self._extended_bond_url_by_iso2_country[
                region.lower()]
            r = self._client.get(url, timeout=25)
            r.raise_for_status()
            if r.status_code != 200:
                return
            bonds = []
            bond_soup = bs4.BeautifulSoup(r.text, 'lxml')
            country_name = bond_soup.find('h1').text.split('-')[0].lstrip().rstrip()
            table_soup = bond_soup.find(
                'table', {'class': 'genTbl closedTbl crossRatesTbl'})
            table_body = table_soup.find('tbody')
            rows = table_body.find_all('tr')
            for row in rows:
                row_column = row.find_all('td')
                duration_label = row_column[1].text.split(' ')[-1]
                bond_yield = float(row_column[2].text)
                duration_month: float
                if duration_label[-1:] is 'W':
                    duration_month = float(duration_label[:-1]) / 4
                elif duration_label[-1:] is 'M':
                    duration_month = float(duration_label[:-1])
                elif duration_label[-1:] is 'Y':
                    duration_month = float(duration_label[:-1]) * 12
                else:
                    print('error parsing maturity')

                b = pyinvesting.Bond(
                    yield_pct=bond_yield,
                    duration_label=duration_label,
                    duration_months=duration_month,
                    country_name=country_name,
                    url=url)
                bonds.append(b)
            return bonds

    def search(self, name, exchange=None):
        r = self._client.post(
            'https://www.investing.com/search/service/search',
            data={
                'search_text': name,
                'term': name,
                'country_id': 0,
                'tab_id': 'All',
            },
            timeout=25)
        r.raise_for_status()
        results = r.json()

        if exchange:
            for result in results['All']:
                if result['symbol'].lower() == name and result['exchange_name_short'].lower() == exchange:
                    return result

        if results['All']:
            return results['All'][0]

        return

    def _get_futures_specs(self):
        for url in self.futures_specs_urls:
            r = self._client.get(url, timeout=25)

            # parse spec table
            spec_soup = bs4.BeautifulSoup(r.content, 'lxml')
            spec_table = spec_soup.find('table', id='specifications')
            spec_rows = spec_table.find_all('tr')[1:]
            for row in spec_rows:
                # Get symbol
                raw_name = row.find(
                    'td', attrs={
                        'class': 'left symbolName'
                    }).contents
                if raw_name:
                    name = re.search(r'\w+', raw_name[0]).group()
                else:
                    continue

                # Get URL
                sym_url = row.find(
                    'td', attrs={
                        'class': 'noWrap bold left'
                    }).a['href']

                info = row.find(
                    'td', attrs={
                        'class': 'noWrap bold left'
                    }).a.contents[0]

                # Get long description
                # Unbelievably specific...
                try:
                    exchange = row.find_all(
                        'td', attrs={'class': 'left'})[2].contents[0]
                except IndexError:
                    continue

                s = pyinvesting.Symbol(
                    name='%s:%s' % (exchange.lower(), name.lower()),
                    info='%s Futures' % info,
                    sym_type='future',
                    url='https://www.investing.com%s' % sym_url,
                    client=self._client,
                )
                self.symbols[s.name] = s

    def get_events_by_date(self, date_from, date_to=None):
        """
        :param date_from: datetime.date in UTC
        :param date_to: datetime.date in UTC
        :return: A list of pyInvesting.event filtered to no be released yet
        """
        if date_to is None:
            date_to = date_from
        data = [
            ('country[]', '35'),  # Japan
            ('country[]', '5'),  # USA
            ('category[]', '_employment'),
            ('category[]', '_economicActivity'),
            ('category[]', '_inflation'),
            ('category[]', '_credit'),
            ('category[]', '_centralBanks'),
            ('category[]', '_confidenceIndex'),
            ('category[]', '_balance'),
            ('category[]', '_Bonds'),
            ('dateFrom', date_from.strftime('%Y-%m-%d')),
            ('dateTo', date_to.strftime('%Y-%m-%d')),
            ('importance[]', '2'),
            ('importance[]', '3'),
            ('timeZone', '55'),  # UTC , #('timeZone', '8') EST
            ('timeFilter', 'timeRemain'),
            ('submitFilters', '1'),
            ('limit_from', '0'),
        ]
        r = self._client.post(
            'https://www.investing.com/economic-calendar/Service/getCalendarFilteredData',
            headers=self._client.headers,
            data=data,
            timeout=25)
        r.raise_for_status()
        data = json.loads(r.content)
        soup = bs4.BeautifulSoup(data['data'], 'lxml')
        # all_days = soup.find_all('td', {'class': "theDay"})
        all_economic_events_soup = soup.find_all('tr',
                                                 {'class': "js-event-item"})
        all_economic_events = []
        for economic_event in all_economic_events_soup:
            if not 'Investing.com' in economic_event.contents[7].text.strip():
                this_dict = {
                    'event_datetime':
                        economic_event.attrs['data-event-datetime'],
                    'event_attr_id':
                        int(economic_event.attrs['event_attr_id']),
                    'release_time':
                        economic_event.contents[1].text,
                    'currency':
                        economic_event.contents[3].text.strip(),
                    'event_volatility':
                        int(economic_event.contents[5].attrs['data-img_key'][-1:]),
                    'event_name':
                        economic_event.contents[7].text.strip(),
                    'actual_release':
                        economic_event.contents[9].text,
                    'forecast_release':
                        economic_event.contents[11].text,
                    'previous_release':
                        economic_event.contents[13].text,
                    'event_url':
                        economic_event.contents[7].contents[0].attrs['href']
                }
                all_economic_events.append(this_dict)
        df = pd.DataFrame(columns=[
            'event_datetime', 'event_attr_id', 'release_time', 'currency',
            'event_volatility', 'event_name', 'actual_release',
            'forecast_release', 'previous_release', 'event_url'
        ])
        pd.options.mode.chained_assignment = None

        for event in all_economic_events:
            df = df.append(
                pd.DataFrame({
                    'event_datetime': event['event_datetime'],
                    'event_attr_id': event['event_attr_id'],
                    'currency': event['currency'],
                    'event_volatility': event['event_volatility'],
                    'event_name': event['event_name'],
                    'actual_release': event['actual_release'],
                    'forecast_release': event['forecast_release'],
                    'previous_release': event['previous_release'],
                    'event_url': event['event_url']
                },
                    index=[0]),
                ignore_index=True,
                sort=True)
        df = df[[
            'event_attr_id', 'currency', 'event_datetime', 'event_name',
            'event_volatility', 'actual_release', 'forecast_release',
            'previous_release', 'event_url'
        ]]
        df['event_volatility'] = pd.to_numeric(
            df.event_volatility, downcast='signed')

        df['forecast_release'] = df['forecast_release'].apply(
            lambda x: None if re.match(r'^\s+$', x) else x)
        df['previous_release'] = df['previous_release'].apply(
            lambda x: None if re.match(r'^\s+$', x) else x)
        df['actual_release'] = df['actual_release'].apply(
            lambda x: None if re.match(r'^\s+$', x) else x)

        events = []
        for event_row in df.to_dict('records'):
            e = pyinvesting.Event(
                name=event_row['event_name'],
                event_id=event_row['event_attr_id'],
                url=event_row['event_url'],
                client=self._client,
                expected=event_row['forecast_release'],
                previous=event_row['previous_release'],
                actual=event_row['actual_release'],
                update_time=datetime.datetime.strptime(
                    event_row['event_datetime'],
                    "%Y/%m/%d %H:%M:%S").replace(tzinfo=pytz.UTC) +
                            relativedelta.relativedelta(seconds=5))
            events.append(e)

        return events

    def get_spx_index(self) -> Index:
        return Index(
            name="S&P500",
            base_url=self.indexman_base,
            index_url="https://www.investing.com/indices/investing.com-us-500-components",
            request_client=self._client
        )


if __name__ == '__main__':
    pass
