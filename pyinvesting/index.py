import bs4
import json
import datetime
from typing import List, Dict, Optional, Tuple


class Constituent(object):
    def __init__(self,
                 name,
                 as_of: datetime.datetime,
                 ticker: str,
                 free_float: float,
                 listed_price: float,
                 weight: float):
        self.name = name
        self.as_of = as_of
        self.ticker = ticker
        self.free_float = free_float
        self.listed_price = listed_price
        self.weight = weight
        self.pair_id = None
        self.point_of_index_daily_move: float = None
        self.last_price = None

    def __repr__(self):
        return self.ticker + " " + self.name + " pdi " + "{0:.2f}".format(self.point_of_index_daily_move)


class Index(object):
    def __init__(self,
                 name: str,
                 base_url: str,
                 index_url: str,
                 request_client):
        self.name = name
        self._base_url = base_url
        self._index_url = index_url
        self._client = request_client
        self._cash_price = None
        self._future_price = None
        self.constituents: List[Constituent] = self.get_constituents()
        self._get_prices()

    def get_constituents(self):
        r = self._client.get(self._base_url + "/indexcompo")
        if r.status_code != 200 or len(r.text) == 2:
            return
        deserialized_json: List[dict] = json.loads(r.content)
        index_composition = list(map(lambda d: Constituent(name=d['name'],
                                                           as_of=datetime.datetime.strptime(d['as_of'], "%Y-%m-%d"),
                                                           ticker=d['ticker'],
                                                           free_float=round(float(d['free_float']), 2),
                                                           listed_price=float(d['listed_price']),
                                                           weight=round(float(d['weight']), 5)),
                                     deserialized_json))
        pair_id_by_ticker = self._get_pair_ids()
        for constituent in index_composition:
            if constituent.ticker in pair_id_by_ticker.keys():
                constituent.pair_id = pair_id_by_ticker[constituent.ticker]
            else:
                print('stock ' + constituent.ticker + ' does not have persisted pair id name')
                index_composition.remove(constituent)
        return index_composition

    def refresh_prices(self):
        self._get_prices()

    def get_movers(self, n: int) -> Tuple[List[Optional[Constituent]], List[Optional[Constituent]]]:
        distinct_point_of_index_moves = {c.point_of_index_daily_move for c in self.constituents}
        if len(distinct_point_of_index_moves) is 1:
            return [None], [None]

        descending_movers = list(sorted(self.constituents,
                                        key=lambda c: c.point_of_index_daily_move,
                                        reverse=True))
        if len(descending_movers) > n:
            return descending_movers[0:n], list(sorted(descending_movers[-n:],
                                                       key=lambda c: c.point_of_index_daily_move))
        else:
            return [None], [None]

    def get_index_px(self) -> Optional[Dict[str, int]]:
        if (self._future_price and self._cash_price) is not None:
            return {
                "future": self._future_price,
                "cash": self._cash_price,
                "basis": self._future_price - self._cash_price
            }
        else:
            return {}

    def _get_pair_ids(self) -> Dict[str, int] or None:
        r = self._client.get(self._base_url + "/transco")
        if r.status_code != 200:
            return
        elif len(r.text) == 2:
            print('empty pair id indexman response')
            return
        return {j["ticker"]: j["pair_id"] for j in json.loads(r.content)}

    @staticmethod
    def _parse_pct_string(str) -> float:
        if str[0] is '+':
            return float(str[1:5]) / 100
        elif str[0] is '-':
            return -1 * float(str[1:5]) / 100
        else:
            return 0.0

    def _get_prices(self):
        r = self._client.get(self._index_url)
        if r.status_code != 200 or len(self.constituents) is 0:
            return

        soup = bs4.BeautifulSoup(r.content, 'lxml')
        index_table = soup.find('table', {'data-gae': 'sb_indices'})
        self._future_price = float(index_table.find('tr', {'pair': '8839'})
                                   .contents[2].text.replace(',', ''))
        self._cash_price = float(index_table.find('tr', {'pair': '166'})
                                 .contents[2].text.replace(',', ''))
        compo_table = soup.find('table', id="cr1")
        rows = compo_table.findAll('tr')[1:]
        pct_x_by_id = {}
        last_px_by_id = {}
        for row in rows:
            pair_id = int(row.attrs['id'].split('_')[1])
            cols = row.findAll('td')
            change = self._parse_pct_string(cols[6].text)
            last_px_by_id[pair_id] = float(cols[2].text.replace(',', ''))
            pct_x_by_id[pair_id] = change

        for constituent in self.constituents:
            pair_id = constituent.pair_id
            if pair_id in pct_x_by_id.keys():
                constituent.last_price = last_px_by_id[pair_id]
                constituent.point_of_index_daily_move = self._cash_price * pct_x_by_id[pair_id] * constituent.weight
        self.constituents = list(filter(lambda c: c.point_of_index_daily_move is not None, self.constituents))
