import datetime
from time import strptime
from dateutil import relativedelta
import bs4
import pytz
from typing import Tuple


class Event(object):
    def __init__(
            self,
            name,
            event_id,
            url,
            client,
            actual=None,
            expected=None,
            previous=None,
            update_time=None,
            retrieved_time=None,
    ):
        self.name = name
        self.actual = actual
        self.expected = expected
        self.previous = previous
        self.update_time = update_time
        self.retrieved_time = retrieved_time
        self._event_id = event_id
        self.url = url
        self._client = client

    def _refresh(self):
        r = self._client.get("https://www.investing.com" + self.url)
        r.raise_for_status()
        if r.status_code != 200:
            return None

        event_soup = bs4.BeautifulSoup(r.content, 'lxml')
        self.name = event_soup.find("title").text
        release_soup = event_soup.find("div", {"id": "releaseInfo"})
        latest_release = release_soup.contents[1].contents[1].text
        date_array = latest_release.split(',')
        last_release_month = date_array[0][:3]
        last_release_day = date_array[0][-2:]
        last_release_year = date_array[1][-4:]
        last_release_datetime = datetime.datetime(
            int(last_release_year),
            strptime(last_release_month, '%b').tm_mon,
            int(last_release_day)) + relativedelta.relativedelta(days=1)
        if last_release_datetime.replace(tzinfo=pytz.UTC) >= self.update_time:
            (self.actual,
             self.previous) = self._parse_event_release_soup(release_soup)
            self.retrieved_time = datetime.datetime.now().replace(
                tzinfo=pytz.UTC)
            return self.actual
        return

    @staticmethod
    def _parse_event_release_soup(release_soup: bs4.BeautifulSoup) -> Tuple[str, str]\
                                                                      or Tuple[None, str]\
                                                                      or Tuple[None, None]:
        try:
            for span in release_soup.find_all('span'):
                if str(span.next) == 'Actual':
                    actual = span.contents[1].text
                elif str(span.next) == 'Previous':
                    previous = span.contents[1].text
            if 'actual' in locals() and 'previous' in locals():
                return actual, previous
            elif 'previous' in locals():
                return None, previous
            return None, None

        except IndexError:
            pass

    def get(self):
        if self.actual is None:
            return self._refresh()
        else:
            return self.actual
