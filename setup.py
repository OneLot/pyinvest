import setuptools

setuptools.setup(
    name="pyinvesting",
    version="2.0",
    author="ANON",
    author_email="ANON@protonmail.com",
    description="A small example package",
    long_description='Rough API around investing.com via web scraping and json api',
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
)
