X_WITH="X-Requested-With: XMLHttpRequest"
UA="User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36"

curl -H "${X_WITH}" -H "${UA}" \
"https://www.investing.com/equities/facebook-inc-options" \
> facebook_optionMaturities.resp

curl -H "${X_WITH}" -H "${UA}" \
"https://www.investing.com/instruments/OptionsDataAjax?pair_id=26490&date=1538715600&strike=all&callsputs=call_and_puts&type=quotes&bringData=true" \
> facebook_option_FDs.resp

curl -H "${X_WITH}" -H "${UA}" \
-H "Content-Type: application/x-www-form-urlencoded" \
-d 'search_text=amd&term=fb&country_id=0&tab_id=All' \
"https://www.investing.com/search/service/search" \
> fb_search.resp

curl -H "${X_WITH}" -H "${UA}" \
"https://www.investing.com/equities/facebook-inc" \
> fb.resp