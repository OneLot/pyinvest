#!/bin/bash

X_WITH="X-Requested-With: XMLHttpRequest"
UA="User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36"

curl -H "${X_WITH}" -H "${UA}" \
	'https://www.investing.com/economic-calendar/Service/getCalendarFilteredData' --data \
	'country%5B%5D=5&category%5B%5D=_employment&category%5B%5D=_economicActivity&category%5B%5D=_inflation&category%5B%5D=_credit&category%5B%5D=_confidenceIndex&category%5B%5D=_balance&category%5B%5D=_Bonds&importance%5B%5D=2&importance%5B%5D=3&dateFrom=2018-11-06&dateTo=2018-11-06&timeZone=8&timeFilter=timeRemain&currentTab=custom&limit_from=0'
