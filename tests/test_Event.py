import datetime
import unittest

import pytz
import requests_mock
from dateutil import relativedelta
import json
import requests

import pyinvesting
import os


class TestEvent(unittest.TestCase):
    @requests_mock.Mocker()
    def setUp(self, mock_client):
        directory_path = os.path.dirname(__file__)
        self.system_path = os.getcwd()
        self.fixture_path = os.path.join(directory_path, 'fixtures/')

        mock_client.get(
            'https://www.investing.com/indices/Service/SpecificationsFutures',
            content=open(self.fixture_path + '/index_futures.resp',
                         'rb').read())

        mock_client.get(
            'https://www.investing.com/commodities/Service/Specifications',
            content=open(self.fixture_path + '/commodity_futures.resp',
                         'rb').read())

        mock_client.get(
            'https://www.investing.com/ratesbonds/Service/SpecificationsFutures',
            content=open(self.fixture_path + '/financial_futures.resp',
                         'rb').read())

        self.pyinvesting = pyinvesting.Client()

    def test_get_event_now_of_future_event_release_assert_actual_is_none_and_event_retrieved_time_is_none(
            self):
        mq = self.pyinvesting
        now = datetime.date.today()
        nextweek = now + relativedelta.relativedelta(weeks=1)
        this_week_events = mq.get_events_by_date(now, nextweek)
        mock_scheduled_event = this_week_events[-1]
        actual = mock_scheduled_event.get()
        self.assertEquals(None, actual)
        self.assertEqual(mock_scheduled_event.retrieved_time, None)

    @requests_mock.Mocker()
    def test_get_event_of_fresh_release(self, m):
        m.get(
            'https://www.investing.com/economic-calendar/jolts-job-openings-1057',
            content=open(self.fixture_path + '/jolts_job_new_release.resp',
                         'rb').read())

        expected_actual = "7.009M"
        event = pyinvesting.Event(
            name='JOLTs Job Openings (Sep)',
            event_id=1057,
            url='/economic-calendar/jolts-job-openings-1057',
            client=requests.Session(),
            expected='7.100M',
            previous='7.293M',
            update_time=datetime.datetime(2018, 11, 6, 10, 0, 0,
                                          0).replace(tzinfo=pytz.UTC))

        actual = event.get()

        self.assertEqual(actual, expected_actual)
        self.assertEqual(event.retrieved_time.date(), datetime.date.today())

    @requests_mock.Mocker()
    def test_get_event_of_non_released_event(self, m):

        m.get(
            'https://www.investing.com/economic-calendar/jolts-job-openings-1057',
            content=open(
                self.fixture_path + '/jolts_job_previous_release.resp',
                'rb').read())

        expected_actual = None
        event = pyinvesting.Event(
            name='JOLTs Job Openings (Sep)',
            event_id=1057,
            url='/economic-calendar/jolts-job-openings-1057',
            client=requests.Session(),
            expected='7.100M',
            previous='7.293M',
            update_time=datetime.datetime(2018, 11, 6, 10, 0, 0,
                                          0).replace(tzinfo=pytz.UTC))
        actual = event.get()

        self.assertEqual(actual, expected_actual)
        self.assertEqual(event.retrieved_time, None)

    def test_get_event_when_event_already_has_actual_expect_actual_release_value(
            self):

        expected_actual = "TOKEN"
        event = pyinvesting.Event(
            client=None,
            name='WSB MAU growth',
            event_id='007',
            url='/uat-economic-calender/core-wsb-inventory-007',
            actual=expected_actual,
            expected='280%',
            previous='253%')
        actual = event.get()

        self.assertEqual(actual, expected_actual)
        self.assertEqual(event.retrieved_time, None)

    @requests_mock.Mocker()
    def test_http_failures(self, m):
        m.register_uri(requests_mock.ANY, requests_mock.ANY, status_code=403)

        event = pyinvesting.Event(
            client=m,
            name='WSB MAU growth',
            event_id='007',
            url='/uat-economic-calender/core-wsb-inventory-007',
            actual=None,
            expected='280%',
            previous='253%')

        with self.assertRaises(Exception):
            event.get()
