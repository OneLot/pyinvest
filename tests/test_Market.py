import unittest
from typing import List

import pytz
import requests_mock
from numpy import float64, int64
import os
import pyinvesting
import re
import json
from dateutil import relativedelta
import datetime


class TestMarketQuotes(unittest.TestCase):

    @requests_mock.Mocker()
    def setUp(self, mock_client):
        directory_path = os.path.dirname(__file__)
        self.system_path = os.getcwd()
        self.fixture_path = os.path.join(directory_path, 'fixtures/')
        self.mock_client = mock_client

        self.mock_client.get(
            'https://www.investing.com/indices/Service/SpecificationsFutures',
            content=open(self.fixture_path + '/index_futures.resp', 'rb').read())

        self.mock_client.get(
            'https://www.investing.com/commodities/Service/Specifications',
            content=open(self.fixture_path + '/commodity_futures.resp', 'rb').read())

        self.mock_client.get(
            'https://www.investing.com/ratesbonds/Service/SpecificationsFutures',
            content=open(self.fixture_path + '/financial_futures.resp', 'rb').read())

        self.mock_pyinvesting = pyinvesting.Client()

    def test_mock_init(self):

        mq = self.mock_pyinvesting
        futures = [
            'cme:es', 'ose:nk', 'comex:gc', 'ice:b',
            'nymex:cl', 'cbot:zb', 'ose:jgb'
        ]

        for symbol in futures:
            assert symbol in mq.symbols.keys()

        self.assertEqual(
            mq.symbols['cme:es'].url,
            'https://www.investing.com/indices/us-spx-500-futures')
        self.assertEqual(mq.symbols['cme:es'].info, 'S&P 500 Futures')
        self.assertEqual(mq.symbols['cme:es'].name, 'cme:es')

        s = mq.get_symbol('cme:es')
        self.assertEqual(s.name, 'cme:es')

        s = mq.get_symbol('es')
        self.assertEqual(s.name, 'cme:es')

    def test_live_init(self):

        mq = self.mock_pyinvesting

        futures = [
            'cme:es', 'ose:nk', 'comex:gc', 'ice:b',
            'nymex:cl', 'cbot:zb', 'ose:jgb'
        ]

        for symbol in futures:
            self.assertIn(symbol, mq.symbols.keys())

        self.assertEqual(
            mq.symbols['cme:es'].url,
            'https://www.investing.com/indices/us-spx-500-futures')
        self.assertEqual(mq.symbols['cme:es'].info, 'S&P 500 Futures')
        self.assertEqual(mq.symbols['cme:es'].name, 'cme:es')

        s = mq.get_symbol('cme:es')
        self.assertEqual(s.name, 'cme:es')

        s = mq.get_symbol('es')
        self.assertEqual(s.name, 'cme:es')

    def test_currency_lookup(self):
        mq = self.mock_pyinvesting

        s = mq.get_symbol('usd/jpy')
        self.assertTrue(re.search('currencies', s.url))

    def test_equity_lookup(self):
        mq = self.mock_pyinvesting

        s = mq.get_symbol('amd')
        self.assertEqual(
            s.url, 'https://www.investing.com/equities/adv-micro-device')

    def test_foreign_equity_lookup(self):
        mq = self.mock_pyinvesting

        s = mq.get_symbol('nse:halc')
        self.assertEqual(
            s.url, 'https://www.investing.com/equities/hindalco-industries')

    def test_fake_lookup(self):
        mq = self.mock_pyinvesting

        s = mq.get_symbol('leeroyjenkins')
        self.assertEqual(s, None)

    def test_futures_short_lookup(self):
        mq = self.mock_pyinvesting

        s = mq.get_symbol('nq')
        self.assertEqual(s.url,
                         'https://www.investing.com/indices/nq-100-futures')
        self.assertEqual(s.info, 'Nasdaq Futures')

    def test_search(self):
        symbols = [
            {'symbol': 'cl',
             'url': 'https://www.investing.com/commodities/crude-oil'
             },
            {
                'symbol': 'nyse:cl',
                'url': 'https://www.investing.com/equities/colgate-palmo'
            }
        ]

        mq = self.mock_pyinvesting

        for symbol in symbols:
            s = mq.get_symbol(symbol['symbol'])
            self.assertAlmostEqual(symbol['url'], s.url)

    def test_pairid(self):
        mq = self.mock_pyinvesting
        s = mq.get_symbol('amd')
        self.assertEqual(s.get_pairid(), '8274')

    def test_get_data_series(self):
        mq = self.mock_pyinvesting
        s = mq.get_symbol('amd')

        # Test mocked
        with requests_mock.Mocker() as m:
            m.get(
                'https://tvc4.forexpros.com/87ec66a24d0f0f35df500ff41071333e/1532480939/1/1/7/history?symbol=8274&resolution=15&from=1532434139&to=1532480939',
                content=open(self.fixture_path + '/amd_data.resp', 'rb').read()
            )
            m.get(
                'https://www.investing.com/equities/adv-micro-device',
                content=open(self.fixture_path + '/amd.resp', 'rb').read(),
            )

            data = s.get_data_series(to_time=1532480939)
            self.assertIsNotNone(data)
            j_data = json.loads(data)
            self.assertIsNotNone(j_data)
            self.assertEqual(j_data['t'][0], 1531153800)

        # Test real calls
        data = s.get_data_series()
        self.assertIsNotNone(json.loads(data))
        self.assertIn(json.loads(data)['s'], ['ok', 'no_data'])

        # Test string resolution call
        data = s.get_data_series(resolution='D')
        self.assertIsNotNone(json.loads(data))
        self.assertEqual(json.loads(data)['s'], 'ok')
        self.assertIn(json.loads(data)['s'], ['ok', 'no_data'])

    def test_get_options_facebook_mock(self):
        mq = self.mock_pyinvesting
        s = mq.get_symbol('fb')
        self.assertEqual(s.get_pairid(), '26490')
        with requests_mock.Mocker() as m:
            m.post(
                'https://www.investing.com/search/service/search',
                content=open(self.fixture_path + '/fb_search.resp', 'rb').read()
            )
            m.get(
                'https://www.investing.com/equities/facebook-inc',
                content=open(self.fixture_path + '/fb.resp', 'rb').read()
            )
            m.get(
                'https://www.investing.com/equities/facebook-inc-options',
                content=open(self.fixture_path + '/facebook_optionMaturities.resp', 'rb').read()
            )
            m.get(
                'https://www.investing.com/instruments/OptionsDataAjax?pair_id=26490&date=1538715600&strike=all&callsputs=call_and_puts&type=quotes&bringData=true',
                content=open(self.fixture_path + '/facebook_option_FDs.resp', 'rb').read()
            )
            (actual_df, j_actual_maturity) = s.get_options(time_dto='FD1', strikes=5)
            self.assertIsNotNone(actual_df)
            j_actual = json.loads(actual_df.to_json(orient='records'))
        expected = [{'call_bid': 12.05, 'call_ask': 12.4, 'call_delta': 0.98, 'call_openint': '99', 'call_volume': '10',
                     'call_theta': '-0.0394', 'call_instrinsic_value': '11.96', 'call_vega': '0.0131',
                     'call_time_value': '-0.26', 'call_rho': '0.0284', 'call_gamma': '0.009', 'call_theo': '12.11',
                     'strike': 152.5, 'put_bid': 0.23, 'put_ask': 0.25, 'put_delta': -0.06, 'put_openint': '1,035',
                     'put_volume': '3,062', 'put_theta': '-0.065', 'put_instrinsic_value': '-11.96',
                     'put_vega': '0.0261', 'put_time_value': '12.16', 'put_rho': '-0.0018', 'put_gamma': '0.0142',
                     'put_theo': '0.2'},
                    {'call_bid': 9.75, 'call_ask': 10.0, 'call_delta': 0.95, 'call_openint': '407',
                     'call_volume': '264', 'call_theta': '-0.0617',
                     'call_instrinsic_value': '9.46', 'call_vega': '0.0251',
                     'call_time_value': '0.24', 'call_rho': '0.028', 'call_gamma': '0.0178',
                     'call_theo': '9.7', 'strike': 155.0, 'put_bid': 0.33, 'put_ask': 0.38,
                     'put_delta': -0.1, 'put_openint': '2,501', 'put_volume': '5,429',
                     'put_theta': '-0.0915', 'put_instrinsic_value': '-9.46', 'put_vega': '0.0386',
                     'put_time_value': '9.81', 'put_rho': '-0.0031', 'put_gamma': '0.0221',
                     'put_theo': '0.35'},
                    {'call_bid': 7.5, 'call_ask': 7.8, 'call_delta': 0.86, 'call_openint': '257', 'call_volume': '87',
                     'call_theta': '-0.1198', 'call_instrinsic_value': '6.96', 'call_vega': '0.0507',
                     'call_time_value': '0.59', 'call_rho': '0.0257', 'call_gamma': '0.0327', 'call_theo': '7.55',
                     'strike': 157.5, 'put_bid': 0.56, 'put_ask': 0.58, 'put_delta': -0.15, 'put_openint': '2,213',
                     'put_volume': '6,845', 'put_theta': '-0.1186', 'put_instrinsic_value': '-6.96',
                     'put_vega': '0.0537', 'put_time_value': '7.54', 'put_rho': '-0.0049', 'put_gamma': '0.0328',
                     'put_theo': '0.58'},
                    {'call_bid': 5.45, 'call_ask': 5.55, 'call_delta': 0.76, 'call_openint': '1,321',
                     'call_volume': '1,831', 'call_theta': '-0.1569', 'call_instrinsic_value': '4.46',
                     'call_vega': '0.0703', 'call_time_value': '1.04', 'call_rho': '0.023', 'call_gamma': '0.0465',
                     'call_theo': '5.5', 'strike': 160.0, 'put_bid': 0.96, 'put_ask': 0.99, 'put_delta': -0.24,
                     'put_openint': '3,114', 'put_volume': '11,984', 'put_theta': '-0.1469',
                     'put_instrinsic_value': '-4.46', 'put_vega': '0.0709', 'put_time_value': '5.44',
                     'put_rho': '-0.0078', 'put_gamma': '0.046', 'put_theo': '0.98'},
                    {'call_bid': 3.6, 'call_ask': 3.7, 'call_delta': 0.64, 'call_openint': '919',
                     'call_volume': '1,704', 'call_theta': '-0.1793', 'call_instrinsic_value': '1.96',
                     'call_vega': '0.0856', 'call_time_value': '1.72', 'call_rho': '0.0193', 'call_gamma': '0.0592',
                     'call_theo': '3.68', 'strike': 162.5, 'put_bid': 1.62, 'put_ask': 1.67, 'put_delta': -0.37,
                     'put_openint': '2,314', 'put_volume': '10,907', 'put_theta': '-0.1658',
                     'put_instrinsic_value': '-1.96', 'put_vega': '0.0856', 'put_time_value': '3.59',
                     'put_rho': '-0.0118', 'put_gamma': '0.059', 'put_theo': '1.63'},
                    {'call_bid': 2.17, 'call_ask': 2.22, 'call_delta': 0.48, 'call_openint': '4,382',
                     'call_volume': '9,195', 'call_theta': '-0.18', 'call_instrinsic_value': '-0.54',
                     'call_vega': '0.0907', 'call_time_value': '2.76', 'call_rho': '0.0147', 'call_gamma': '0.0654',
                     'call_theo': '2.22', 'strike': 165.0, 'put_bid': 2.64, 'put_ask': 2.68, 'put_delta': -0.52,
                     'put_openint': '3,181', 'put_volume': '8,068', 'put_theta': '-0.1664',
                     'put_instrinsic_value': '0.54', 'put_vega': '0.0907', 'put_time_value': '2.13',
                     'put_rho': '-0.0169', 'put_gamma': '0.0651', 'put_theo': '2.67'},
                    {'call_bid': 1.15, 'call_ask': 1.18, 'call_delta': 0.31, 'call_openint': '2,916',
                     'call_volume': '10,496', 'call_theta': '-0.1511', 'call_instrinsic_value': '-3.04',
                     'call_vega': '0.0808', 'call_time_value': '4.19', 'call_rho': '0.0097', 'call_gamma': '0.0613',
                     'call_theo': '1.15', 'strike': 167.5, 'put_bid': 4.1, 'put_ask': 4.25, 'put_delta': -0.68,
                     'put_openint': '2,851', 'put_volume': '5,519', 'put_theta': '-0.1456',
                     'put_instrinsic_value': '3.04', 'put_vega': '0.0817', 'put_time_value': '1.15',
                     'put_rho': '-0.0222', 'put_gamma': '0.059', 'put_theo': '4.19'},
                    {'call_bid': 0.53, 'call_ask': 0.56, 'call_delta': 0.18, 'call_openint': '7,536',
                     'call_volume': '17,567', 'call_theta': '-0.1112', 'call_instrinsic_value': '-5.54',
                     'call_vega': '0.0601', 'call_time_value': '6.1', 'call_rho': '0.0056', 'call_gamma': '0.0457',
                     'call_theo': '0.56', 'strike': 170.0, 'put_bid': 5.95, 'put_ask': 6.2, 'put_delta': -0.78,
                     'put_openint': '3,082', 'put_volume': '6,513', 'put_theta': '-0.132',
                     'put_instrinsic_value': '5.54', 'put_vega': '0.0676', 'put_time_value': '0.74',
                     'put_rho': '-0.0258', 'put_gamma': '0.0438', 'put_theo': '6.28'},
                    {'call_bid': 0.23, 'call_ask': 0.27, 'call_delta': 0.1, 'call_openint': '4,517',
                     'call_volume': '8,075', 'call_theta': '-0.0746', 'call_instrinsic_value': '-8.04',
                     'call_vega': '0.0394', 'call_time_value': '8.31', 'call_rho': '0.003', 'call_gamma': '0.0293',
                     'call_theo': '0.27', 'strike': 172.5, 'put_bid': 8.15, 'put_ask': 8.45, 'put_delta': -0.84,
                     'put_openint': '1,842', 'put_volume': '1,117', 'put_theta': '-0.1177',
                     'put_instrinsic_value': '8.04', 'put_vega': '0.055', 'put_time_value': '0.51',
                     'put_rho': '-0.0282', 'put_gamma': '0.0319', 'put_theo': '8.55'},
                    {'call_bid': 0.11, 'call_ask': 0.12, 'call_delta': 0.05, 'call_openint': '8,711',
                     'call_volume': '11,236', 'call_theta': '-0.044', 'call_instrinsic_value': '-10.54',
                     'call_vega': '0.0229', 'call_time_value': '10.66', 'call_rho': '0.0015', 'call_gamma': '0.0167',
                     'call_theo': '0.12', 'strike': 175.0, 'put_bid': 10.45, 'put_ask': 10.8, 'put_delta': -0.9,
                     'put_openint': '1,321', 'put_volume': '7,398', 'put_theta': '-0.0866',
                     'put_instrinsic_value': '10.54', 'put_vega': '0.0408', 'put_time_value': '0.27',
                     'put_rho': '-0.0304', 'put_gamma': '0.0228', 'put_theo': '10.81'}]

        count = 0
        for i in expected:
            self.assertEqual(i["call_bid"], j_actual[count]["call_bid"])
            self.assertEqual(i["call_ask"], j_actual[count]["call_ask"])
            self.assertEqual(i["call_delta"], j_actual[count]["call_delta"])
            self.assertEqual(i["call_openint"], j_actual[count]["call_openint"])
            self.assertEqual(i["call_volume"], j_actual[count]["call_volume"])
            self.assertEqual(i["call_theta"], j_actual[count]["call_theta"])
            self.assertEqual(i["call_instrinsic_value"], j_actual[count]["call_instrinsic_value"])
            self.assertEqual(i["call_vega"], j_actual[count]["call_vega"])
            self.assertEqual(i["call_time_value"], j_actual[count]["call_time_value"])
            self.assertEqual(i["call_rho"], j_actual[count]["call_rho"])
            self.assertEqual(i["call_gamma"], j_actual[count]["call_gamma"])
            self.assertEqual(i["call_theo"], j_actual[count]["call_theo"])
            self.assertEqual(i["strike"], j_actual[count]["strike"])
            self.assertEqual(i["put_bid"], j_actual[count]["put_bid"])
            self.assertEqual(i["put_ask"], j_actual[count]["put_ask"])
            self.assertEqual(i["put_delta"], j_actual[count]["put_delta"])
            self.assertEqual(i["put_openint"], j_actual[count]["put_openint"])
            self.assertEqual(i["put_volume"], j_actual[count]["put_volume"])
            self.assertEqual(i["put_theta"], j_actual[count]["put_theta"])
            self.assertEqual(i["put_instrinsic_value"], j_actual[count]["put_instrinsic_value"])
            self.assertEqual(i["put_vega"], j_actual[count]["put_vega"])
            self.assertEqual(i["put_time_value"], j_actual[count]["put_time_value"])
            self.assertEqual(i["put_rho"], j_actual[count]["put_rho"])
            self.assertEqual(i["put_gamma"], j_actual[count]["put_gamma"])
            self.assertEqual(i["put_theo"], j_actual[count]["put_theo"])
            count += 1
        self.assertIsInstance(j_actual_maturity, int)

    def test_get_options_different_equities_weekly_options_live(self):
        mq = self.mock_pyinvesting
        s = mq.get_symbol('fb')
        s1 = mq.get_symbol('amzn')
        s2 = mq.get_symbol('pbr')

        (facebook_options_df, facebook_option_maturity) = s.get_options(time_dto='FD1', strikes=5)
        (amazon_option_df, amazon_option_maturity) = s1.get_options(time_dto='FD1', strikes=5)
        (petrobras_options_df, petrobras_option_maturity) = s2.get_options(time_dto='FD1', strikes=5)

        self.assertEqual(facebook_options_df.index.size, 10)
        self.assertEqual(amazon_option_df.index.size, 10)
        self.assertEqual(petrobras_options_df.index.size, 10)

    def test_get_facebook_options_with_different_monthly_maturity_live(self):
        mq = self.mock_pyinvesting
        s = mq.get_symbol('fb')
        (facebook_current_month_options, facebook_current_monthly_expiry) = s.get_options(time_dto='M0', strikes=5)
        (facebook_next_monthly_options, facebook_next_monthly_expiry) = s.get_options(time_dto='M1', strikes=5)
        (facebook_next_two_monthly_options, facebook_next_two_monthly_expiry) = s.get_options(time_dto='M2', strikes=5)
        (facebook_next_three_monthly_options, facebook_next_three_monthly_expiry) = s.get_options(time_dto='M3', strikes=5)

        self.assertTrue(facebook_next_monthly_expiry < facebook_next_two_monthly_expiry)
        self.assertTrue(type(facebook_current_monthly_expiry) is int or facebook_current_monthly_expiry is None)
        self.assertTrue(type(facebook_next_three_monthly_expiry) is int or facebook_next_three_monthly_expiry is None)

    def test_get_options_equity_without_options_live(self):
        mq = self.mock_pyinvesting
        s = mq.get_symbol('tlry')
        price = s.quote()
        actual_df, maturity_epoch = s.get_options(time_dto='FD1', strikes=5)

        self.assertIsNotNone(price)
        self.assertEqual(actual_df, maturity_epoch)
        self.assertEqual(actual_df, None)

    def test_get_options_with_spot_over_one_thousand_live(self):
        mq = self.mock_pyinvesting
        s = mq.get_symbol('amzn')
        actual_df, maturity_epoch = s.get_options(time_dto='M1', strikes=5)
        mid_strike_len = len(actual_df.strike) // 2

        self.assertTrue(actual_df is not None)
        self.assertTrue(type(maturity_epoch) is int)
        print('amzn maturity ' + str(maturity_epoch) + ' with mid strike ' + str(actual_df.strike.iat[mid_strike_len]))

    def test_get_events_by_date_live(self):
        mq = self.mock_pyinvesting
        now = datetime.date.today()
        nextweek = now + relativedelta.relativedelta(weeks=1)
        events = mq.get_events_by_date(now, nextweek)
        self.assertTrue(type(events) == type([pyinvesting.Event]))
        for i in range(len(events)):
            self.assertTrue(type(events[i].update_time) == datetime.datetime)
            self.assertTrue(type(events[i].update_time.tzinfo) == type(pytz.UTC))

    @requests_mock.Mocker()
    def test_get_events_by_date(self, mock_client):
        mq = self.mock_pyinvesting
        test_data = json.load(open(self.fixture_path + '/events_data.json'))

        mock_client.post(
            'https://www.investing.com/economic-calendar/Service/getCalendarFilteredData',
            json=test_data['mock_data'])

        event_data = [
            {
                "name": "JOLTs Job Openings  (Sep)",
                "url": "/economic-calendar/jolts-job-openings-1057",
                "actual": "7.009M",
                "expected": "7.100M",
                "previous": "7.293M",
                "update_time": "2018-11-06 10:00:05+00:00"
            },
            {
                "name": "10-Year Note Auction",
                "url": "/economic-calendar/10-year-note-auction-571",
                "actual": "3.209%",
                "expected": None,
                "previous": "3.225%",
                "update_time": "2018-11-06 13:00:05+00:00"
            },
            {
                "name": "API Weekly Crude Oil Stock",
                "url": "/economic-calendar/api-weekly-crude-stock-656",
                "actual": None,
                "expected": None,
                "previous": "5.700M",
                "update_time": "2018-11-06 16:30:05+00:00",
            }
        ]
        events = mq.get_events_by_date(date_from=datetime.date.today())

        self.assertEqual(len(events), len(event_data), msg="Event count mismatch")
        request = mock_client.last_request
        self.assertRegexpMatches(request.text,r'dateFrom=%s' % (datetime.date.today()), msg="Event query date mismatch")

        for i in range(len(event_data)):
            event_truth = event_data[i]
            event_resp = events[i]
            self.assertEquals(event_truth['name'],event_resp.name)
            self.assertEquals(event_truth['expected'],event_resp.expected, msg="%s expected does not match" % (event_truth['name']))
            self.assertEquals(event_truth['previous'],event_resp.previous, msg="%s previous does not match" % (event_truth['name']))
            self.assertEquals(event_truth['update_time'],str(event_resp.update_time), msg="%s update_time does not match" % (event_truth['name']))
            self.assertEquals(event_truth['url'],event_resp.url, msg="%s url does not match" % (event_truth['name']))

    def test_get_bonds_us(self):
        mq = self.mock_pyinvesting
        url = 'https://www.investing.com/rates-bonds/usa-government-bonds'
        with requests_mock.Mocker() as m:
            m.get(
                url,
                content=open(self.fixture_path + '/bonds_us_govt.resp', 'rb').read())
            actual_bonds: List[pyinvesting.Bond] = mq.get_bonds(region='us')
        expected_bonds = [
            pyinvesting.Bond(
                duration_label='1M',
                duration_months=1.0,
                yield_pct=2.385,
                url=url,
                region='usa'
            ),
            pyinvesting.Bond(
                duration_label='3M',
                duration_months=3.0,
                yield_pct=2.383,
                url=url,
                region='usa'
            ),
            pyinvesting.Bond(
                duration_label='6M',
                duration_months=6.0,
                yield_pct=2.512,
                url=url,
                region='usa'
            ),
            pyinvesting.Bond(
                duration_label='1Y',
                duration_months=12.0,
                yield_pct=2.599,
                url=url,
                region='usa'
            ),
            pyinvesting.Bond(
                duration_label='2Y',
                duration_months=24.0,
                yield_pct=2.611,
                url=url,
                region='usa'
            ),
            pyinvesting.Bond(
                duration_label='3Y',
                duration_months=36.0,
                yield_pct=2.588,
                url=url,
                region='usa'
            ),
            pyinvesting.Bond(
                duration_label='5Y',
                duration_months=60.0,
                yield_pct=2.598,
                url=url,
                region='usa'
            ),
            pyinvesting.Bond(
                duration_label='7Y',
                duration_months=84.0,
                yield_pct=2.667,
                url=url,
                region='usa'
            ),
            pyinvesting.Bond(
                duration_label='10Y',
                duration_months=120.0,
                yield_pct=2.759,
                url=url,
                region='usa'
            ),
            pyinvesting.Bond(
                duration_label='30Y',
                duration_months=360.0,
                yield_pct=3.066,
                url=url,
                region='usa'
            )
        ]

        self.assertTrue(len(actual_bonds) == len(expected_bonds))
        count = 0
        for expceted_bond in expected_bonds:
            self.assertEqual(expceted_bond.duration_label, actual_bonds[count].duration_label, msg="%s does not match" % expceted_bond.duration_label)
            self.assertEqual(expceted_bond.duration_months, actual_bonds[count].duration_months, msg="%s does not match" % str(expceted_bond.duration_months))
            self.assertEqual(expceted_bond.yield_pct, actual_bonds[count].yield_pct, msg="%s does not match" % str(expceted_bond.yield_pct))
            count = count + 1

    @requests_mock.Mocker()
    def test_http_failures(self, mock_client):
        mock_client.register_uri(requests_mock.ANY,requests_mock.ANY, status_code=403)
        
        with self.assertRaises(Exception):
            self.mock_pyinvesting.search("fail")

        with self.assertRaises(Exception):
            self.mock_pyinvesting.get_bonds("us")

        with self.assertRaises(Exception):
            self.mock_pyinvesting.get_events_by_date(date_from=datetime.date.today())

