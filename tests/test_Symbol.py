import unittest
import requests
import requests_mock
import pyinvesting
import os


class TestSymbol(unittest.TestCase):
    def setUp(self):
        directory_path = os.path.dirname(__file__)
        self.system_path = os.getcwd()
        self.fixture_path = os.path.join(directory_path, 'fixtures/')

    @requests_mock.Mocker()
    def test_equity_quote(self, m):
        m.get(
            'https://www.investing.com/equities/adv-micro-device',
            content=open(self.fixture_path + '/amd.resp', 'rb').read())
        s = pyinvesting.Symbol(
            name='amd',
            info='Advanced Micro Devices',
            url='https://www.investing.com/equities/adv-micro-device',
            client=requests.session(),
        )

        quote = s.quote()
        quote_data = {
            'last': '15.670',
            'changed': '+0.820',
            'changed_pct': '+5.52%',
            'ah_last': '15.670',
            'ah_changed': '0.000',
            'ah_changed_pct': '0.000%'
        }
        for key in quote_data.keys():
            self.assertEqual(quote[key], quote_data[key])

        overview = s.overview()
        overview_data = {
            'Prev. Close': '14.85',
            "Day's Range": '15.04 - 15.74',
            'Revenue': '6.98B',
            'Open': '15.07',
            '52 wk Range': '9.04 - 15.74',
            'EPS': ' - 0.68',
            'Volume': '97,089,040',
            'Market Cap': '14.39B',
            'Dividend (Yield)': 'N/A (N/A)',
            'Average Vol. (3m)': '60,428,129',
            'P/E Ratio': 'N/A',
            'Beta': '2.82',
            '1-Year Change': '30.26%',
            'Shares Outstanding': '969,000,000',
            'Next Earnings Date': 'Jul 23, 2018'
        }

        for key in overview_data:
            self.assertEqual(overview[key], overview_data[key])

    @requests_mock.Mocker()
    def test_equity_quote_pm(self, m):
        m.get(
            'https://www.investing.com/equities/adv-micro-device',
            content=open(self.fixture_path + '/amd_pm.resp', 'rb').read())
        s = pyinvesting.Symbol(
            name='amd',
            info='Advanced Micro Devices',
            url='https://www.investing.com/equities/adv-micro-device',
            client=requests.session(),
        )

        quote = s.quote()
        quote_data = {
            'last': '15.250',
            'changed': '+0.360',
            'changed_pct': '+2.42%',
            'pm_last': '15.210',
            'pm_changed': '-0.040',
            'pm_changed_pct': '-0.262%'
        }
        for key in quote_data.keys():
            self.assertEqual(quote[key], quote_data[key])

        overview = s.overview()
        overview_data = {
            'Prev. Close': '14.89',
            "Day's Range": '14.31 - 15.33',
            'Revenue': '6.98B',
            'Open': '14.52',
            '52 wk Range': '9.04 - 15.97',
            'EPS': ' - 0.68',
            'Volume': '81,930,486',
            'Market Cap': '14.78B',
            'Dividend (Yield)': 'N/A (N/A)',
            'Average Vol. (3m)': '60,428,129',
            'P/E Ratio': 'N/A',
            'Beta': '2.83',
            '1-Year Change': '24.19%',
            'Shares Outstanding': '969,000,000',
            'Next Earnings Date': 'Jul 23, 2018'
        }

        for key in overview_data:
            self.assertEqual(overview[key], overview_data[key])

    @requests_mock.Mocker()
    def test_future_quote(self, m):
        m.get(
            'https://www.investing.com/indices/us-spx-500-futures',
            content=open(self.fixture_path + '/es.resp', 'rb').read())

        s = pyinvesting.Symbol(
            name='cme:es',
            info='S&P 500 Futures',
            url='https://www.investing.com/indices/us-spx-500-futures',
            client=requests.session(),
        )

        quote = s.quote()
        quote_data = {
            'last': '2,774.50',
            'changed': '+2.25',
            'changed_pct': '+0.08%'
        }
        self.assertEqual(quote, quote_data)

        overview = s.overview()
        overview_data = {
            'Prev. Close': '2,772.25',
            'Month': 'Jun 18',
            'Tick Size': '0.25',
            'Open': '2,773.5',
            'Contract Size': '$50 x Index Price',
            'Tick Value': '12.5',
            "Day's Range": '2,770.25 - 2,774.75',
            'Settlement Type': 'Cash',
            'Base Symbol': 'ES',
            '52 wk Range': '2,402.25 - 2,878.5',
            'Settlement Day': '06/15/2018',
            'Point Value': '1 = $50',
            '1-Year Change': '14.05%',
            'Last Rollover Day': '03/11/2018',
            'Months': 'HMUZ'
        }
        for key in quote_data.keys():
            self.assertEqual(quote[key], quote_data[key])

        for key in overview_data:
            self.assertEqual(overview[key], overview_data[key])

    @requests_mock.Mocker()
    def test_http_failures(self, m):
        m.register_uri(requests_mock.ANY, requests_mock.ANY, status_code=403)

        s = pyinvesting.Symbol(
            name='amd',
            info='Advanced Micro Devices',
            url='https://www.investing.com/equities/adv-micro-device',
            client=requests.session(),
        )

        with self.assertRaises(Exception):
            s.quote()
        with self.assertRaises(Exception):
            s.overview()
        with self.assertRaises(Exception):
            s.get_data_series()
        with self.assertRaises(Exception):
            s.get_options()
        with self.assertRaises(Exception):
            s.get_pairid()