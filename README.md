Background:
```
pyInvesting is a private repo I forked and worked along side another developer.
Since joining, we added options, bonds, and economic releases, 
which are imported/leveraged through a marketbot to visualize and offer free market data on RFQ or economic release announcements in chat channels.
```

Basic example usage:

```
$ python
Python 3.6.3 |Anaconda, Inc.| (default, Oct  6 2017, 12:04:38)
[GCC 4.2.1 Compatible Clang 4.0.1 (tags/RELEASE_401/final)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> import pyinvesting
>>> c = pyinvesting.Client()
>>> s = c.get_symbol('fb')
>>> s.quote()
{'last': '157.33', 'changed': '-1.52', 'changed_pct': '-0.96%', 'ah_last': '157.04', 'ah_changed': '-0.29', 'ah_changed_pct': '-0.18%'}
>>> s.get_options("fd2",5)
(    call_bid  call_ask  call_delta   ...     put_rho put_gamma put_theo
13     13.55     13.70        0.86   ...     -0.0063    0.0168     0.08
14      9.20      9.25        0.77   ...     -0.0121    0.0284     0.45
15      7.25      7.35        0.70   ...     -0.0165    0.0347     0.91
16      5.50      5.60        0.62   ...     -0.0219    0.0403     1.64
17      4.00      4.10        0.52   ...     -0.0282    0.0435     2.71
18      2.83      2.87        0.42   ...     -0.0348    0.0435     4.12
19      1.90      1.93        0.32   ...     -0.0414    0.0404     5.87
20      1.23      1.25        0.23   ...     -0.0474    0.0338     7.87
21      0.77      0.79        0.16   ...     -0.0527    0.0255    10.08
22      0.48      0.49        0.11   ...     -0.0569    0.0167    12.42

[10 rows x 25 columns], 1539925200)
>>> s.overview()
{'Prev. Close': '158.85', "Day's Range": '156.2 - 160.9', 'Revenue': '48.5B', 'Open': '159.21', '52 wk Range': '149.02 - 218.62', 'EPS': '7.24', 'Volume': '25,744,047', 'Market Cap': '458.63B', 'Dividend (Yield)': 'N/A (N/A)', 'Average Vol. (3m)': '24,416,101', 'P/E Ratio': '21.95', 'Beta': '0.47', '1-Year Change': ' - 8.12%', 'Shares Outstanding': '2,887,218,664', 'Next Earnings Date': 'Oct 30, 2018'}
```